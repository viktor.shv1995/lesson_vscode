from typing import Union

from fastapi import FastAPI

from external_1.add import add_one

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": add_one(item_id), "q": q}
